// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllPlayerController.h"
#include "TimerManager.h"
#include "Blueprint/UserWidget.h"

void AKillEmAllPlayerController::BeginPlay() {
	Super::BeginPlay();
	HUD = CreateWidget(this, HUDClass);
	if (HUD != nullptr)
	{
		HUD->AddToViewport();
	}

}

void AKillEmAllPlayerController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);
	UUserWidget* EndGameScreen;
	HUD->RemoveFromViewport();
	if (bIsWinner)
		EndGameScreen = CreateWidget(this, WinWidgetClass);
	else
		EndGameScreen = CreateWidget(this, LoseWidgetClass);

	if (EndGameScreen != nullptr)
	{
		EndGameScreen->AddToViewport();
	}

	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);

	//ChangeWidget()
	//UE_LOG(LogTemp, Warning, TEXT("Game has ended"));
}