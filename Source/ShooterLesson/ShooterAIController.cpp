// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "ShooterCharacter.h"

bool AShooterAIController::IsDead() const
{
	AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn());

	if (ControlledCharacter != nullptr)
	{
		return ControlledCharacter->IsDead();
	}

	return true;
}

void AShooterAIController::BeginPlay()
{
	Super::BeginPlay();
	if (AIBehaviour != nullptr)
	{
		RunBehaviorTree(AIBehaviour);
	}

	PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (PlayerPawn != nullptr)
	{
		GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"), PlayerPawn->GetActorLocation());
		GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), GetPawn()->GetActorLocation());
	}
}

/*void AShooterAIController::BeginPlay()
{
	Super::BeginPlay();
	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	SetFocus(PlayerPawn);
}*/

//void AShooterAIController::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//	//APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
//
//	if (LineOfSightTo(PlayerPawn))
//	{
//		//we are setting playerLocation becasue we see the player and follow him sale thing as the last known Player location
//		GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"), PlayerPawn->GetActorLocation());
//		GetBlackboardComponent()->SetValueAsVector(TEXT("LastKnownPlayerLocation"), PlayerPawn->GetActorLocation());
//	}
//	else
//	{
//		//we are clearing Player location so we don't get chased but searched
//		GetBlackboardComponent()->ClearValue(TEXT("PlayerLocation"));
//	}
//
//	//if lineofSight
//		//MoveTo
//		//SetFocus
//	//else
//		//ClearFocus
//		//StopMovement
//}