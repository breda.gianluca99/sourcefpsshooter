// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "ShooterAIController.h"

void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);

	//UE_LOG(LogTemp, Warning, TEXT("A Pawn has been killed"))

	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());

	if (PlayerController != nullptr)
	{
		EndGame(false);
	}

	for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (!AIController->IsDead())
		{
			return;
		}
	}
	
	EndGame(true);

	//For Loop Sugli Enemy
		//controlleremo se sono morti
			//return
	//Endgame

}
void AKillEmAllGameMode::EndGame(bool bIsPlayerWinner)
{

	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		//bool isPlayerController = Controller->IsPlayerController();
		/*
		if (bIsPlayerWinner)
		{			Controller->GameHasEnded(nullptr, isPlayerController);		}
		else
		{			Controller->GameHasEnded(nullptr, !isPlayerController);		}*/

		bool isWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), isWinner);
	}
}