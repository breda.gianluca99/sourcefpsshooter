// Fill out your copyright notice in the Description page of Project Settings.

#include "KOTHShooterAIController.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "CapturePoint.h"
#include "ShooterCharacter.h"

void AKOTHShooterAIController::BeginPlay()
{
	Super::BeginPlay();
	AActor* CapturePointActor;
	CapturePointActor = UGameplayStatics::GetActorOfClass(GetWorld(), ACapturePoint::StaticClass());
	if (CapturePointActor != nullptr)
	{
		GetBlackboardComponent()->SetValueAsVector(TEXT("CapturePoint"), CapturePointActor->GetActorLocation());
	}
}