// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

#define OUT

// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
	InitializeWeapon();
}


AController* AGun::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr)
		return nullptr;
	return OwnerPawn->GetController();
}


bool AGun::GunTrace(FHitResult& Hit OUT, FVector& ShotDirection OUT)
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr)
		return false;

	FVector Location;
	FRotator Rotation;
	OwnerController->GetPlayerViewPoint(Location, Rotation);
	ShotDirection = -Rotation.Vector();

	FVector End = Location + Rotation.Vector() * MaxRange;

	//DrawDebugCamera(GetWorld(), Location, Rotation, 90, 2, FColor::Magenta, true);
	//TODO: LineTrace
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	return GetWorld()->LineTraceSingleByChannel(Hit, Location, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGun::PullTrigger()
{
	if (!DecreaseCurrCLip())
	{
		ReloadGun();
		return;
	}

	//we spawn the particle system at the location named MuzzleFlash
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));

	FHitResult Hit;
	FVector ShotDirection;
	bool bSuccess = GunTrace(Hit OUT, ShotDirection OUT);
	if (!bSuccess) return;


	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, ShotDirection.Rotation());
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), HitSound, Hit.Location);
	//if(Hit.GetActor()->Collision)

	AActor* HitActor = Hit.GetActor();
	/*UPrimitiveComponent* PrimComponent = Cast<UPrimitiveComponent>(HitActor->GetRootComponent());
	if (PrimComponent != nullptr)
	{
		FName profileName = PrimComponent->GetCollisionProfileName();
		ECollisionChannel collChannel;
		FCollisionResponseParams collResponseParams;
		UCollisionProfile::GetChannelAndResponseParams(profileName, collChannel, collResponseParams);
		UE_LOG(LogTemp, Warning, TEXT("Collision Channel: %s"), *profileName.ToString())
	}*/

	if (HitActor != nullptr)
	{
		FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
		AController* OwnerController = GetOwnerController();
		HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
	}
}
bool AGun::DecreaseCurrCLip()
{
	if (CurrClip-- > 0)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Current Clip left %d"), CurrClip);
		//UE_LOG(LogTemp, Warning, TEXT("Current Ammo left %d"), CurrAmmo);
		return true;
	}
	else
		return false;
}

void AGun::ReloadGun()
{
	//if the total ammo left minus the clip size is higher or equal to the ammo used in the clip
	if ((CurrAmmo - ClipSize) >= 0)
	{
		//then we get the used ammo in the clip
		int ClipUsedAmmo = ClipSize - CurrClip;

		//if the total ammo left in the gun is higher or eaqual to the Ammo used in the clip 
		//we reload the current clip to get the current clip to full clip size
		//else we add all total ammo left 
		CurrClip += CurrAmmo >= ClipUsedAmmo ? ClipUsedAmmo : CurrAmmo;

		//then we remove from the current total ammo the ammo that were used
		CurrAmmo -= ClipUsedAmmo;

		//fMath min tra ammo della clip e le ammo della pouch togli da uno l'altro
	}
	else
	{
		CurrAmmo = 0;
	}
}

void AGun::AddAmmo(int AmmoClips)
{
	for (int i = 0; i < AmmoClips; i++)
	{
		if ((CurrAmmo + ClipSize) < MaxAmmo)
		{
			CurrAmmo += ClipSize;
		}
		else {
			CurrAmmo = MaxAmmo;
		}
	}
}

void AGun::InitializeWeapon()
{
	bCanShoot = true;
	CurrClip = ClipSize;
	CurrAmmo = MaxAmmo - ClipSize;
}

int AGun::GetCurrentClip() const
{
	return CurrClip;
}

int AGun::GetCurrentAmmo() const
{
	return CurrAmmo;
}

void AGun::CanShoot()
{
	bCanShoot = true;
}

bool AGun::GetCanShoot()
{
	return bCanShoot;
}

void AGun::SetCanShoot(bool NewValue)
{
	bCanShoot = NewValue;
}

float AGun::GetRateOfFire() 
{
	return RateOfFire;
}