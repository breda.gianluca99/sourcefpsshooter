// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gun.generated.h"

UCLASS()
class SHOOTERLESSON_API AGun : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGun();

	void ReloadGun();
	virtual void PullTrigger();
	bool DecreaseCurrCLip();
	bool GetCanShoot();
	void SetCanShoot(bool NewValue);
	float GetRateOfFire();
	void CanShoot();
	void AddAmmo(int AmmoClips);
	int GetCurrentClip() const;
	int GetCurrentAmmo() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);
	void InitializeWeapon();

	AController* GetOwnerController() const;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	UPROPERTY(EditAnywhere)
		USoundBase* MuzzleSound;

	UPROPERTY(EditAnywhere)
		USoundBase* HitSound;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
		UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere)
		UParticleSystem* ImpactEffect;

	UPROPERTY(EditAnywhere)
		float MaxRange = 1500;

	UPROPERTY(EditAnywhere)
		float Damage = 10;

	UPROPERTY(EditAnywhere)
		int ClipSize = 12;

	UPROPERTY(EditAnywhere)
		int MaxAmmo = 60;

	int CurrClip;
	int CurrAmmo;
	bool bCanShoot = true;
	UPROPERTY(EditAnywhere)
		float RateOfFire = 25.f;

	//UPROPERTY(EditAnywhere)
	//	float ReloadTimer = 3;

};
