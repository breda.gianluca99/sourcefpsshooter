// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "Pellet.h"
#include "Launcher.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERLESSON_API ALauncher : public AGun
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	ALauncher();

public:
	UPROPERTY(EditAnywhere, Category = "Projectile")
		class TSubclassOf<class APellet> Pellet;

	UFUNCTION()
		virtual void PullTrigger() override;

	UPROPERTY(EditAnywhere)
		float 	PalletOffsetSpawn = 150.f;
};
