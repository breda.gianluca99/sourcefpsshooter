// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "KillEmAllPlayerController.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERLESSON_API AKillEmAllPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	void GameHasEnded(class AActor* EndGameFocus = nullptr, bool bIsWinner = false) override;
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
		float RestartDelay = 5;

	//void ChangeWidget(TSubclassOf<UUserWidget>NewWidgetClass);

	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> HUDClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> LoseWidgetClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> WinWidgetClass;

	UPROPERTY(EditAnywhere)
		UUserWidget* HUD;
	FTimerHandle RestartTimer;
};