// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp.h"

// Sets default values
APickUp::APickUp()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	Root->Mobility = EComponentMobility::Type::Movable;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	//we setup the Capsule Component for overlap
	OverlapCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Overlap PickUp Capsule"));
	OverlapCapsule->SetGenerateOverlapEvents(true);
	OverlapCapsule->SetupAttachment(Root);

	RootComponent = Root;
}

// Called when the game starts or when spawned
void APickUp::BeginPlay()
{
	Super::BeginPlay();
	//funziona anche con OnActorBeginOverlap se mesh 
	OverlapCapsule->OnComponentBeginOverlap.AddDynamic(this, &APickUp::OnOverlapBegin);
}

// Called every frame
void APickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickUp::OnOverlapBegin
(
	class UPrimitiveComponent* OverlappedComp,
	class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult
)
{
	//UE_LOG(LogTemp, Error, TEXT("BeginOverlap"));
	Character = Cast<AShooterCharacter>(OtherActor);
	if (Character)
	{
		Character->RefillAmmo(Clips);
		//OverlapCapsule->SetGenerateOverlapEvents(false);
		this->Destroy();
	}
}