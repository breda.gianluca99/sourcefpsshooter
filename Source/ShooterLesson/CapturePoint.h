// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterCharacter.h"
#include "TimerManager.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "CapturePoint.generated.h"

UCLASS()
class SHOOTERLESSON_API ACapturePoint : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACapturePoint();
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void AddPointsToScore();
	void AddToScore(AShooterCharacter* Actor);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Overlap Capsule")
		class UCapsuleComponent* CollisionComponent;

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
		class USceneComponent* Root;

	UPROPERTY(EditAnywhere)
		float RestartDelay = 5;

	FTimerHandle TimerCheck;
	AShooterCharacter* CharInsideSphere;
};
