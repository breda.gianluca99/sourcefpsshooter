// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterLessonGameModeBase.h"
#include "TimerManager.h"
#include "KOTHGameMode.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERLESSON_API AKOTHGameMode : public AShooterLessonGameModeBase
{
	GENERATED_BODY()

public:
	virtual void PawnKilled(APawn* PawnKilled) override;
	virtual void StartPlay() override;
	void EndGame(bool bIsPlayerWinner);
	void CheckResult();
	UPROPERTY(EditAnywhere)
		float MatchTime = 60.f;

	FTimerHandle TimerCheck;
};
