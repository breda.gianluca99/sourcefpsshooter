// Fill out your copyright notice in the Description page of Project Settings.


#include "CapturePoint.h"

// Sets default values
ACapturePoint::ACapturePoint()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	Root->Mobility = EComponentMobility::Type::Movable;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	CollisionComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Component"));
	CollisionComponent->SetGenerateOverlapEvents(true);
	CollisionComponent->SetupAttachment(Root);

	RootComponent = Root;
}

void ACapturePoint::OnOverlapBegin
(
	UPrimitiveComponent* OverlappedComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult
)
{
	UE_LOG(LogTemp, Error, TEXT("BeginOverlap"));
	CharInsideSphere = Cast<AShooterCharacter>(OtherActor);
	if (CharInsideSphere)
	{
		//AddToScore(InsideSphere);//errore
		GetWorldTimerManager().SetTimer(TimerCheck, this, &ACapturePoint::AddPointsToScore, RestartDelay, true, RestartDelay);
	}
}

void ACapturePoint::OnOverlapEnd(
	class UPrimitiveComponent* OverlappedComp,
	class AActor* OtherActor,
	class UPrimitiveComponent*
	OtherComp, int32 OtherBodyIndex
)
{
	UE_LOG(LogTemp, Error, TEXT("EndOverlap"));
	CharInsideSphere = nullptr;
	GetWorldTimerManager().ClearTimer(TimerCheck);
}

// Called when the game starts or when spawned
void ACapturePoint::BeginPlay()
{
	Super::BeginPlay();

	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &ACapturePoint::OnOverlapBegin);
	CollisionComponent->OnComponentEndOverlap.AddDynamic(this, &ACapturePoint::OnOverlapEnd);
}

void ACapturePoint::AddPointsToScore()
{
	if (CharInsideSphere)
	{
		//UE_LOG(LogTemp, Error, TEXT("AddedPoints 2"));
		AddToScore(CharInsideSphere);
	}
}

void ACapturePoint::AddToScore(AShooterCharacter* Actor)
{
	if (Actor)
	{
		//UE_LOG(LogTemp, Error, TEXT("AddedPoints 1"));
		Actor->AddScore(1);
	}
}

// Called every frame
void ACapturePoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}