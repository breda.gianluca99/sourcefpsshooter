// Fill out your copyright notice in the Description page of Project Settings.


#include "KOTHGameMode.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "ShooterCharacter.h"
#include "ShooterAIController.h"//needs to be changed


void AKOTHGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);

	//UE_LOG(LogTemp, Warning, TEXT("A Pawn has been killed"))

	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());

	if (PlayerController != nullptr)
	{
		//Substitute with respawn
		EndGame(false);
	}

	for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (!AIController->IsDead())
		{
			//Substitute with respawn
			return;
		}
	}

	EndGame(true);

	//For Loop Sugli Enemy
		//controlleremo se sono morti
			//return
	//Endgame

}
void AKOTHGameMode::EndGame(bool bIsPlayerWinner)
{
	//dobbiamo tirarci fuori gli score e compararli
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		//bool isPlayerController = Controller->IsPlayerController();
		/*
		if (bIsPlayerWinner)
		{			Controller->GameHasEnded(nullptr, isPlayerController);		}
		else
		{			Controller->GameHasEnded(nullptr, !isPlayerController);		}*/

		GetWorldTimerManager().ClearTimer(TimerCheck);

		bool isWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		Controller->GameHasEnded(Controller->GetPawn(), isWinner);
	}
}

void AKOTHGameMode::StartPlay()
{
	Super::StartPlay();

	//We add a timer
	GetWorldTimerManager().SetTimer(TimerCheck, this, &AKOTHGameMode::CheckResult, MatchTime, true);
}

void AKOTHGameMode::CheckResult()
{
	float PlayerScore = 0;
	float AIScore = 0;
	bool IsPlayerWinner = false;

	APlayerController* PlayerController = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());

	if (PlayerController != nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Player"));
		AShooterCharacter* Char = Cast<AShooterCharacter>(PlayerController->GetCharacter());
		PlayerScore = Char->GetScore();
	}

	for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
	{
		UE_LOG(LogTemp, Error, TEXT("AI"));
		AShooterCharacter* AIChar = Cast<AShooterCharacter>(AIController->GetCharacter());
		if (PlayerScore >= AIChar->GetScore())
		{
			IsPlayerWinner = true;
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("The boolean value IsPlayerWinner is %s"), (IsPlayerWinner ? TEXT("true") : TEXT("false")));

	EndGame(IsPlayerWinner);
}