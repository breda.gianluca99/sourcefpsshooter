// Fill out your copyright notice in the Description page of Project Settings.


#include "Launcher.h"

ALauncher::ALauncher()
{

}

void ALauncher::PullTrigger()
{
	if (!DecreaseCurrCLip())
	{
		ReloadGun();
		return;
	}


	if (GetCanShoot() == true) {
		UE_LOG(LogTemp, Error, TEXT("Boom"));
		FVector PlayerLocation = GetOwner()->GetActorLocation();
		FVector ForwardVector = GetOwner()->GetActorForwardVector();
		FVector PalletLocSpawn = (ForwardVector * PalletOffsetSpawn) + PlayerLocation;
		FRotator PlayerRotation = GetOwner()->GetActorRotation();

		UE_LOG(LogTemp, Error, TEXT("FW %s"), *PalletLocSpawn.ToString());

		APellet* Projectile = GetWorld()->SpawnActor<APellet>(Pellet, PalletLocSpawn, PlayerRotation);
		if (Projectile) {
			UE_LOG(LogTemp, Error, TEXT("Direzione in cui spariamo %s"), *ForwardVector.ToString());
			Projectile->ShootAtDirection(ForwardVector);
		}
		SetCanShoot(false);
		FTimerHandle TimerHandle;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &ALauncher::CanShoot, GetRateOfFire(), false);
	}
}
