// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ShooterAIController.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERLESSON_API AShooterAIController : public AAIController
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere)
		class UBehaviorTree* AIBehaviour;
	APawn* PlayerPawn;
public:
	bool IsDead() const;
	//UPROPERTY(EditAnywhere)
	//	class UBlackboardComponent* AIBlackboard;
	virtual void BeginPlay() override;
};
