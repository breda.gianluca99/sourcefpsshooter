// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gun.h"
#include "Components/CapsuleComponent.h"
#include "ShooterLessonGameModeBase.h"

bool AShooterCharacter::IsDead() const
{
	return Health <= 0;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

int AShooterCharacter::GetScore() const
{
	return Score;
}

void AShooterCharacter::AddScore(int toAdd)
{
	Score += toAdd;
}

int AShooterCharacter::GetCurrentClip() const
{
	return Gun->GetCurrentClip();
	//return FString::FromInt(Gun->GetCurrentClip());
}

int AShooterCharacter::GetCurrentAmmo() const
{
	return Gun->GetCurrentAmmo();
	//return FString::FromInt(Gun->GetCurrentAmmo());
}

// Sets default values
AShooterCharacter::AShooterCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	Health = MaxHealth;
	Score = 0;
	GunIndex = 0;
	StartSetupGuns();
}

void AShooterCharacter::StartSetupGuns()
{
	//We attach all guns and set them to hidden
	for (int i = 0; i < GunsClass.Num(); i++)
	{
		//we instanciate the gun
		Gun = GetWorld()->SpawnActor<AGun>(GunsClass[i]);

		//we attach the gun
		GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
		Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
		Gun->SetOwner(this);
		//we set it to hidden
		Gun->SetActorHiddenInGame(true);
		Guns.Add(Gun);
	}
	Gun = Guns[GunIndex];
	Gun->SetActorHiddenInGame(false);
}

void AShooterCharacter::ChangeWeapon(float AxisValue)
{
	/*
		GunIndex = Guns.Num() - 1AttachGun;
		_selectedCard = 0;
	*/
	//UE_LOG(LogTemp, Warning, TEXT("Axis t: %d"), GunIndex);
	GunIndex += AxisValue;

	if (GunIndex < 0) // this is for looping cycle of hand selection
	{
		GunIndex = Guns.Num() - 1;
	}
	else if (GunIndex > Guns.Num() - 1)
	{
		GunIndex = 0;
	}
	Gun->SetActorHiddenInGame(true);
	Gun = Guns[GunIndex];
	Gun->SetActorHiddenInGame(false);
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);
	PlayerInputComponent->BindAxis(TEXT("MouseWheel"), this, &AShooterCharacter::ChangeWeapon);
	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Shoot"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Shoot);
	PlayerInputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Reload);
}

void AShooterCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::LookUpRate(float AxisValue)
{
	AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookRightRate(float AxisValue)
{
	AddControllerYawInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::Shoot()
{
	Gun->PullTrigger();
}

void AShooterCharacter::Reload()
{
	Gun->ReloadGun();
}

void AShooterCharacter::RefillAmmo(int ClipCount)
{
	Gun->AddAmmo(ClipCount);
}

float AShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	DamageToApply = FMath::Min(Health, DamageToApply);
	Health -= DamageToApply;

	UE_LOG(LogTemp, Warning, TEXT("Health left %f"), Health);

	if (IsDead())
	{
		//we need to remove the killed for good to a skecthy respawn system
		AShooterLessonGameModeBase* GameMode = GetWorld()->GetAuthGameMode<AShooterLessonGameModeBase>();
		if (GameMode != nullptr)
		{
			GameMode->PawnKilled(this);
		}

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	return DamageToApply;
}