
#include "Kismet/GameplayStatics.h"
#include "Pellet.h"

// Sets default values
APellet::APellet()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	Root->Mobility = EComponentMobility::Type::Movable;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	CollisionComponent->InitSphereRadius(SphereRadius);
	CollisionComponent->SetGenerateOverlapEvents(true);
	CollisionComponent->SetupAttachment(Root);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(CollisionComponent);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
	ProjectileMovementComponent->InitialSpeed = 1000.f;
	ProjectileMovementComponent->MaxSpeed = 1000.f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	//ProjectileMovementComponent->ProjectileGravityScale = 0.0f;

	RootComponent = Root;
	InitialLifeSpan = 5.f;
}

//// Called when the game starts or when spawned
//void APellet::BeginPlay()
//{
//	Super::BeginPlay();
//	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &APellet::OnOverlapBegin);
//}

void APellet::ShootAtDirection(const FVector& ShootDirection)
{
	ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
	//Sound
	UE_LOG(LogTemp, Error, TEXT("Velocity %s"), *(ShootDirection * ProjectileMovementComponent->InitialSpeed).ToString());
}


void APellet::OnOverlapBegin
(
	class UPrimitiveComponent* OverlappedComp,
	class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult
)
{
	//We spawn a particle system

	//Sound of Hit And Explosion

	//We Deal the damage to the people

	TArray<FHitResult> HitInSphere;
	FVector MyLocation = GetActorLocation();
	FCollisionShape CollSphere = FCollisionShape::MakeSphere(ExplosionRadius);
	FCollisionQueryParams QueryParams;
	FCollisionResponseParams ResponseParams;

	bool bIsHit = GetWorld()->SweepMultiByChannel
	(
		OUT HitInSphere,
		MyLocation,
		MyLocation,
		FQuat::Identity,
		ECollisionChannel::ECC_GameTraceChannel1,//to edit
		CollSphere,
		QueryParams,
		ResponseParams
	);

	//for (auto& Hit : HitInSphere) {
	//
	//	if (Hit != nullptr)
	//	{
	//
	//		//FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
	//		//AController* OwnerController = GetOwnerController();
	//		//HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
	//		//FPointDamageEvent DamageEvent(100.f, Hit, ShotDirection, nullptr);
	//		//AController* OwnerController = GetOwnerController();
	//		//HitActor->TakeDamage(100.f, DamageEvent, OwnerController, this);
	//
	//	}
	//}

	//UE_LOG(LogTemp, Error, TEXT("BeginOverlap"));
	//Character = Cast<AShooterCharacter>(OtherActor);
	//if (Character)
	//{
	//	Character->RefillAmmo(Clips);
	//	//OverlapCapsule->SetGenerateOverlapEvents(false);
	//	this->Destroy();
	//}
}