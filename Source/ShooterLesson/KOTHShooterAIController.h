// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterAIController.h"
#include "KOTHShooterAIController.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERLESSON_API AKOTHShooterAIController : public AShooterAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
};
